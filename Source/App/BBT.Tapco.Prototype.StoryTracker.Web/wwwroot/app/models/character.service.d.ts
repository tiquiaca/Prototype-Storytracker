import { Http } from '@angular/http';
import { Character } from './character.model';
import { ExceptionService, MessageService, SpinnerService } from '../core';
export declare class CharacterService {
    private http;
    private exceptionService;
    private messageService;
    private spinnerService;
    onDbReset: any;
    constructor(http: Http, exceptionService: ExceptionService, messageService: MessageService, spinnerService: SpinnerService);
    addCharacter(character: Character): any;
    deleteCharacter(character: Character): any;
    getCharacters(): any;
    getCharacter(id: number): any;
    updateCharacter(character: Character): any;
    private extractData<T>(res);
}

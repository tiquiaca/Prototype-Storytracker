export declare class EntityService {
    clone<T>(source: T): T;
    merge: (target: any, ...sources: any[]) => any;
    propertiesDiffer: (entityA: {}, entityB: {}) => string;
}

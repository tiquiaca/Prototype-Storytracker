export let CONFIG = {
  baseUrls: {
    config: 'commands/config',
    resetDb: 'commands/resetDb',
    characters: 'http://localhost:15535/api/characters/get',
    vehicles: 'api/vehicles.json'
  }
};

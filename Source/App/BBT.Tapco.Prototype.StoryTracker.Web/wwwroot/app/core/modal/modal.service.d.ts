export declare class ModalService {
    activate: (message?: string, title?: string) => Promise<boolean>;
}

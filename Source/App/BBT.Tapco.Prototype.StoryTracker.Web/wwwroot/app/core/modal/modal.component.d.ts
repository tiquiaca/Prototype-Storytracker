import { OnInit } from '@angular/core';
import { ModalService } from './modal.service';
export declare class ModalComponent implements OnInit {
    title: string;
    message: string;
    okText: string;
    cancelText: string;
    negativeOnClick: (e: any) => void;
    positiveOnClick: (e: any) => void;
    private defaults;
    private modalElement;
    private cancelButton;
    private okButton;
    constructor(modalService: ModalService);
    activate(message?: string, title?: string): Promise<boolean>;
    ngOnInit(): void;
    private show();
    private hideDialog();
}

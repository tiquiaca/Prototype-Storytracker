import { Http } from '@angular/http';
import { Vehicle } from './vehicle.model';
import { ExceptionService, MessageService, SpinnerService } from '../../core';
export declare class VehicleService {
    private http;
    private exceptionService;
    private messageService;
    private spinnerService;
    onDbReset: any;
    constructor(http: Http, exceptionService: ExceptionService, messageService: MessageService, spinnerService: SpinnerService);
    addVehicle(vehicle: Vehicle): any;
    deleteVehicle(vehicle: Vehicle): any;
    getVehicles(): any;
    private extractData<T>(res);
    getVehicle(id: number): any;
    updateVehicle(vehicle: Vehicle): any;
}

import { OnDestroy, OnInit } from '@angular/core';
import { Character, CharacterService } from '../../models';
import { FilterTextComponent } from '../../shared/filter-text/filter-text.component';
import { FilterTextService } from '../../shared/filter-text/filter-text.service';
export declare class CharacterListComponent implements OnDestroy, OnInit {
    private characterService;
    private filterService;
    private dbResetSubscription;
    characters: Character[];
    filteredCharacters: Character[];
    filterComponent: FilterTextComponent;
    constructor(characterService: CharacterService, filterService: FilterTextService);
    filterChanged(searchText: string): void;
    getCharacters(): void;
    ngOnDestroy(): void;
    ngOnInit(): void;
    trackByCharacters(index: number, character: Character): number;
}

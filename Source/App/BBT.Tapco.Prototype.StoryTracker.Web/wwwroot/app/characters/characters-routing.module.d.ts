import { CharacterListComponent } from './character-list/character-list.component';
import { CharacterComponent } from './character/character.component';
import { CharactersComponent } from './characters.component';
export declare class CharactersRoutingModule {
}
export declare const routedComponents: (typeof CharacterComponent | typeof CharacterListComponent | typeof CharactersComponent)[];

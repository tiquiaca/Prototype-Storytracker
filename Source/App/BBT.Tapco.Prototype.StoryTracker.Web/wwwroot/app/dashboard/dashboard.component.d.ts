import { OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Character, CharacterService } from '../../app/models';
import { ToastService } from '../../app/core';
export declare class DashboardComponent implements OnDestroy, OnInit {
    private route;
    private characterService;
    private router;
    private toastService;
    private dbResetSubscription;
    characters: Observable<Character[]>;
    title: string;
    constructor(route: ActivatedRoute, characterService: CharacterService, router: Router, toastService: ToastService);
    getCharacters(): void;
    gotoDetail(character: Character): void;
    ngOnDestroy(): void;
    ngOnInit(): void;
    trackByCharacters(index: number, character: Character): number;
}

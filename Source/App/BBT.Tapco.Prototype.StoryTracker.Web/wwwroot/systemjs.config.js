﻿/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
    System.config({
        paths: {
            // paths serve as alias
            'libs:': 'libs/'
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            'app': 'app',
            'main': 'main.js',
            // angular bundles
            '@angular/core': 'libs:@angular/core/bundles/core.umd.js',
            '@angular/core/testing': 'libs:@angular/core/bundles/core-testing.umd.js',
            '@angular/common': 'libs:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'libs:@angular/compiler/bundles/compiler.umd.js',
            '@angular/compiler/testing': 'libs:@angular/compiler/bundles/compiler-testing.umd.js',
            '@angular/platform-browser': 'libs:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser/testing': 'libs:@angular/platform-browser/bundles/platform-browser-testing.umd.js',
            '@angular/platform-browser-dynamic': 'libs:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/platform-browser-dynamic/testing': 'libs:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic-testing.umd.js',
            '@angular/http': 'libs:@angular/http/bundles/http.umd.js',
            '@angular/http/testing': 'libs:@angular/http/bundles/http-testing.umd.js',
            '@angular/router': 'libs:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'libs:@angular/forms/bundles/forms.umd.js',

            //Still have issues with some dependencies on my machine
            //will configure in the future
            //better to have all installed thru bower, or other package manager
            //  '@angular/material': 'libs:@angular/material/material.umd.js',
            // other libraries
            'rxjs': 'libs:rxjs',
            'angular-in-memory-web-api': 'libs:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js'
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            'app/core': { main: 'index' }, // PAPA
            'app/models': { main: 'index' }, // PAPA
            '': { main: 'main.js', defaultExtension: 'js' },
            'app': { main: 'app.module.js', defaultExtension: 'js' },
            'api': { defaultExtension: 'js' },
            'rxjs': { defaultExtension: 'js' },

            //app: {
            //    main: '../main.js',
            //    defaultExtension: 'js'
            //},
            //rxjs: {
            //    defaultExtension: 'js'
            //}
        }
    });
})(this);

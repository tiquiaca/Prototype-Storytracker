export declare class InMemoryStoreService {
    createDb(): {
        characters: {
            "id": number;
            "name": string;
            "side": string;
        }[];
        vehicles: {
            "id": number;
            "name": string;
            "type": string;
        }[];
    };
}

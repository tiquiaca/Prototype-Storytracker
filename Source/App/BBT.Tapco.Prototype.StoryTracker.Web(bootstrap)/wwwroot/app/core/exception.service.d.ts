import { Observable } from 'rxjs/Observable';
import { ToastService } from './toast/toast.service';
export declare class ExceptionService {
    private toastService;
    constructor(toastService: ToastService);
    catchBadResponse: (errorResponse: any) => Observable<any>;
}

export interface SpinnerState {
    show: boolean;
}
export declare class SpinnerService {
    private spinnerSubject;
    spinnerState: any;
    constructor(prior: SpinnerService);
    show(): void;
    hide(): void;
}

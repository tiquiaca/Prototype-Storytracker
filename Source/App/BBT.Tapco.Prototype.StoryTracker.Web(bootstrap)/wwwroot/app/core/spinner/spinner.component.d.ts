import { OnDestroy, OnInit } from '@angular/core';
import { SpinnerService } from './spinner.service';
export declare class SpinnerComponent implements OnDestroy, OnInit {
    private spinnerService;
    visible: boolean;
    private spinnerStateChanged;
    constructor(spinnerService: SpinnerService);
    ngOnInit(): void;
    ngOnDestroy(): void;
}

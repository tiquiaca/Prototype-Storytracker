import { CanActivate, CanActivateChild, CanLoad, Route, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserProfileService } from './user-profile.service';
export declare class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
    private userProfileService;
    private router;
    constructor(userProfileService: UserProfileService, router: Router);
    canLoad(route: Route): boolean;
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean;
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean;
}

import { SpinnerService, UserProfileService } from '../../app/core';
export declare class LoginService {
    private spinnerService;
    private userProfileService;
    constructor(spinnerService: SpinnerService, userProfileService: UserProfileService);
    login(): any;
    logout(): void;
    private toggleLogState(val);
}

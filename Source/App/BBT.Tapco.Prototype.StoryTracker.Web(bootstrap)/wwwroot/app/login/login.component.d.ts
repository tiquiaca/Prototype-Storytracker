import { OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from './login.service';
import { ToastService, UserProfileService } from '../core';
export declare class LoginComponent implements OnDestroy {
    private loginService;
    private route;
    private router;
    private toastService;
    private userProfileService;
    private loginSub;
    constructor(loginService: LoginService, route: ActivatedRoute, router: Router, toastService: ToastService, userProfileService: UserProfileService);
    readonly isLoggedIn: boolean;
    login(): void;
    logout(): void;
    ngOnDestroy(): void;
}

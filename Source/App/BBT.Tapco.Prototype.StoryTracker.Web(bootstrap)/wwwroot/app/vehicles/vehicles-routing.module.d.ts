import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { VehiclesComponent } from './vehicles.component';
export declare class VehiclesRoutingModule {
}
export declare const routedComponents: (typeof VehicleComponent | typeof VehicleListComponent | typeof VehiclesComponent)[];

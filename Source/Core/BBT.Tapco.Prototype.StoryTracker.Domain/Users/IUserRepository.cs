﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Users
{/// <summary>
/// Licuanan, John James
/// 12/21/2016
/// User repository to be implement on UserRepository
/// </summary>
    public interface IUserRepository
    {
        void Add(User user);

        void Update(User user);

        void Delete(User user);

        IEnumerable<User> Get();

        UserId GetNextUserId();
    }
}

﻿using System;
using BBT.Tapco.Prototype.Domain.Common;
using BBT.Tapco.Prototype.Domain.Common.Modeling;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles
{
    public class Vehicle : Entity, IEquatable<Vehicle>
    {
        private string _name;
        private string _type;

        public Vehicle()
        { }

        public Vehicle(VehicleId vehicleId, string name, string type)
        {
            AssertionConcern.AssertArgumentNotNull(vehicleId, @"Cannot instantiate a vehicle without an identity!");
            AssertionConcern.AssertArgumentNotNull(name, @"Cannot instantiate a vehicle without a name!");
            AssertionConcern.AssertArgumentNotNull(type, @"Cannot instantiate a vehicle without a type!");

            VehicleId = vehicleId;
            _name = name;
            _type = type;
        }
        
        public VehicleId VehicleId { get; set; }
        
        public string Name
        {
            get { return _name; }
            set
            {
                AssertionConcern.AssertArgumentNotNull(value, "Cannot set the vehicle name to null!");
                _name = value;
            }
        }

        public string Type
        {
            get { return _type; }
            set
            {
                AssertionConcern.AssertArgumentNotNull(value, "Cannot set the vehicle type to null!");
                _type = value;
            }
        }

        public bool Equals(Vehicle other)
        {
            return VehicleId.Equals(other.VehicleId);
        }
    }
}
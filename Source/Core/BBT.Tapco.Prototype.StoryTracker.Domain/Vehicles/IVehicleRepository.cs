﻿using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles
{
    public interface IVehicleRepository
    {
        bool Add(Vehicle vehicle);

        bool Update(Vehicle vehicle);

        bool Delete(Vehicle vehicle);

        IEnumerable<Vehicle> Get();

        VehicleId GetNextVehicleId();
        Vehicle GetVehicleById(int id);
    }
}
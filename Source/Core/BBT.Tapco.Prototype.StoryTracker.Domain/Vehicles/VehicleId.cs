﻿using BBT.Tapco.Prototype.Domain.Common.Modeling;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles
{
    public class VehicleId : Identity
    {
        public VehicleId(int identifier) : base(identifier.ToString())
        {

        }

        public int Identifier => int.Parse(Id);
    }
}

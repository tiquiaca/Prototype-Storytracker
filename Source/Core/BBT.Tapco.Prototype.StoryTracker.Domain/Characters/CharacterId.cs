﻿using BBT.Tapco.Prototype.Domain.Common.Modeling;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Characters
{
    public class CharacterId : Identity
    {
        public CharacterId(int identifier) : base(identifier.ToString())
        {

        }

        public int Identifier => int.Parse(Id);
    }
}
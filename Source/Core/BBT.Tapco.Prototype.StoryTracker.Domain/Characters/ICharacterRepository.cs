﻿using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Domain.Characters
{
    public interface ICharacterRepository
    {
        bool Add(Character character);

        void Update(Character character);

        bool Delete(Character character);

        IEnumerable<Character> Get();

        CharacterId GetNextCharacterId();

        Character GetCharacterById(int id);
    }
}

﻿using System;
using System.Diagnostics.Tracing;

namespace BBT.Tapco.Prototype.StoryTracker.Domain
{
    [EventSource(Name = "BBT-Tapco-Prototype-StoryTracker")]
    public class StoryTrackerEventSource : EventSource
    {
        private static readonly Lazy<StoryTrackerEventSource> Source = new Lazy<StoryTrackerEventSource>();

        public class Tasks
        {
            //Character
            public const EventTask RetrieveCharacterList = (EventTask) 1;
            public const EventTask AddCharacter = (EventTask) 2;
            public const EventTask UpdateCharacter = (EventTask)3;
            public const EventTask DeleteCharacter = (EventTask)4;
            //Vehicle
            public const EventTask RetrieveVehicleList = (EventTask)5;
            public const EventTask AddVehicle = (EventTask)6;
            public const EventTask UpdateVehicle = (EventTask)7;
            public const EventTask DeleteVehicle = (EventTask)8;
        }

        public static StoryTrackerEventSource Log => Source.Value;

        [NonEvent]
        private void Write(int id, params object[] args)
        {
            if (IsEnabled())
            {
                WriteEvent(id, args);
            }
        }


        #region GetCharacter
        [Event(1,
           Message =
               "The StoryTracker api received a request to retrieve all characters.",
           Level = EventLevel.Informational, Task = Tasks.RetrieveCharacterList, Opcode = EventOpcode.Start)]
        public void StartTheRequestWasReceivedToReturnEveryCharacter()
        {
            Write(1);
        }

        [Event(2,
           Message =
               "Completed the request to retrieve all characters.",
           Level = EventLevel.Informational, Task = Tasks.RetrieveCharacterList, Opcode = EventOpcode.Stop)]
        public void StopTheRequestWasReceivedToReturnEveryCharacter()
        {
            Write(2);
        }

        [Event(3,
           Message =
               "An error occurred while retrieving the character list with message: {0} and stack: {1}.",
           Level = EventLevel.Error, Task = Tasks.RetrieveCharacterList)]
        public void ErrorWhileExecutingRequestToReturnEveryCharacter(string message, string stackTrace)
        {
            Write(3, message, stackTrace);
        }

        #endregion

        #region AddCharacter
        [Event(4,
          Message =
              "The StoryTracker api received a request to add a character.",
          Level = EventLevel.Informational, Task = Tasks.AddCharacter, Opcode = EventOpcode.Start)]
        public void StartTheRequestWasReceivedToAddCharacter()
        {
            Write(4);
        }

        [Event(5,
           Message =
               "Completed the request to add a character.",
           Level = EventLevel.Informational, Task = Tasks.AddCharacter, Opcode = EventOpcode.Stop)]
        public void StopTheRequestWasReceivedToAddCharacter()
        {
            Write(2);
        }

        [Event(6,
           Message =
               "An error occurred while adding the character list with message: {0} and stack: {1}.",
           Level = EventLevel.Error, Task = Tasks.AddCharacter)]
        public void ErrorWhileExecutingRequestToAddCharacter(string message, string stackTrace)
        {
            Write(6, message, stackTrace);
        }
        #endregion

        #region UpdateCaharacter
        [Event(7,
         Message =
             "The StoryTracker api received a request to update a character.",
         Level = EventLevel.Informational, Task = Tasks.UpdateCharacter, Opcode = EventOpcode.Start)]
        public void StartTheRequestWasReceivedToUpdateCharacter()
        {
            Write(7);
        }

        [Event(8,
           Message =
               "Completed the request to update a character.",
           Level = EventLevel.Informational, Task = Tasks.UpdateCharacter, Opcode = EventOpcode.Stop)]
        public void StopTheRequestWasReceivedToUpdateCharacter()
        {
            Write(8);
        }

        [Event(9,
           Message =
               "An error occurred while updating the character list with message: {0} and stack: {1}.",
           Level = EventLevel.Error, Task = Tasks.UpdateCharacter)]
        public void ErrorWhileExecutingRequestToUpdateCharacter(string message, string stackTrace)
        {
            Write(9, message, stackTrace);
        }
        #endregion

        #region DeleteCharacter
        [Event(10,
        Message =
            "The StoryTracker api received a request to delete a character.",
        Level = EventLevel.Informational, Task = Tasks.DeleteCharacter, Opcode = EventOpcode.Start)]
        public void StartTheRequestWasReceivedToDeleteCharacter()
        {
            Write(10);
        }

        [Event(11,
           Message =
               "Completed the request to delete a character.",
           Level = EventLevel.Informational, Task = Tasks.DeleteCharacter, Opcode = EventOpcode.Stop)]
        public void StopTheRequestWasReceivedToDeleteCharacter()
        {
            Write(11);
        }

        [Event(12,
           Message =
               "An error occurred while deleting the character list with message: {0} and stack: {1}.",
           Level = EventLevel.Error, Task = Tasks.DeleteCharacter)]
        public void ErrorWhileExecutingRequestToDeleteCharacter(string message, string stackTrace)
        {
            Write(12, message, stackTrace);
        }
        #endregion




        #region GetVehicle
        [Event(13,
           Message =
               "The StoryTracker api received a request to retrieve all characters.",
           Level = EventLevel.Informational, Task = Tasks.RetrieveVehicleList, Opcode = EventOpcode.Start)]
        public void StartTheRequestWasReceivedToReturnEveryVehicle()
        {
            Write(13);
        }

        [Event(14,
           Message =
               "Completed the request to retrieve all characters.",
           Level = EventLevel.Informational, Task = Tasks.RetrieveVehicleList, Opcode = EventOpcode.Stop)]
        public void StopTheRequestWasReceivedToReturnEveryVehicle()
        {
            Write(14);
        }

        [Event(15,
           Message =
               "An error occurred while retrieving the character list with message: {0} and stack: {1}.",
           Level = EventLevel.Error, Task = Tasks.RetrieveVehicleList)]
        public void ErrorWhileExecutingRequestToReturnEveryVehicle(string message, string stackTrace)
        {
            Write(15, message, stackTrace);
        }

        #endregion

        #region AddVehicle
        [Event(16,
          Message =
              "The StoryTracker api received a request to add a character.",
          Level = EventLevel.Informational, Task = Tasks.AddVehicle, Opcode = EventOpcode.Start)]
        public void StartTheRequestWasReceivedToAddVehicle()
        {
            Write(16);
        }

        [Event(17,
           Message =
               "Completed the request to add a character.",
           Level = EventLevel.Informational, Task = Tasks.AddVehicle, Opcode = EventOpcode.Stop)]
        public void StopTheRequestWasReceivedToAddVehicle()
        {
            Write(17);
        }

        [Event(18,
           Message =
               "An error occurred while adding the character list with message: {0} and stack: {1}.",
           Level = EventLevel.Error, Task = Tasks.AddVehicle)]
        public void ErrorWhileExecutingRequestToAddVehicle(string message, string stackTrace)
        {
            Write(18, message, stackTrace);
        }
        #endregion

        #region UpdateVehicle
        [Event(19,
         Message =
             "The StoryTracker api received a request to update a character.",
         Level = EventLevel.Informational, Task = Tasks.UpdateVehicle, Opcode = EventOpcode.Start)]
        public void StartTheRequestWasReceivedToUpdateVehicle()
        {
            Write(19);
        }

        [Event(20,
           Message =
               "Completed the request to update a character.",
           Level = EventLevel.Informational, Task = Tasks.UpdateVehicle, Opcode = EventOpcode.Stop)]
        public void StopTheRequestWasReceivedToUpdateVehicle()
        {
            Write(20);
        }

        [Event(21,
           Message =
               "An error occurred while updating the character list with message: {0} and stack: {1}.",
           Level = EventLevel.Error, Task = Tasks.UpdateVehicle)]
        public void ErrorWhileExecutingRequestToUpdateVehicle(string message, string stackTrace)
        {
            Write(21, message, stackTrace);
        }
        #endregion

        #region DeleteVehicle
        [Event(22,
        Message =
            "The StoryTracker api received a request to delete a character.",
        Level = EventLevel.Informational, Task = Tasks.DeleteVehicle, Opcode = EventOpcode.Start)]
        public void StartTheRequestWasReceivedToDeleteVehicle()
        {
            Write(22);
        }

        [Event(23,
           Message =
               "Completed the request to delete a character.",
           Level = EventLevel.Informational, Task = Tasks.DeleteVehicle, Opcode = EventOpcode.Stop)]
        public void StopTheRequestWasReceivedToDeleteVehicle()
        {
            Write(23);
        }

        [Event(24,
           Message =
               "An error occurred while deleting the character list with message: {0} and stack: {1}.",
           Level = EventLevel.Error, Task = Tasks.DeleteVehicle)]
        public void ErrorWhileExecutingRequestToDeleteVehicle(string message, string stackTrace)
        {
            Write(24, message, stackTrace);
        }
        #endregion



    }
}
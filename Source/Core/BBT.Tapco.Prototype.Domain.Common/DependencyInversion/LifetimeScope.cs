﻿namespace BBT.Tapco.Prototype.Domain.Common.DependencyInversion
{
    public enum LifetimeScope
    {
        Transient = 0, //new instance jshelton
        Singleton
    }
}
﻿using System;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.Domain.Common.DependencyInversion
{
    public interface IDependencyContainer : IDisposable
    {
        void Register(params InjectionModule[] injectionModules);
        T Resolve<T>();
        T Resolve<T>(string name);
        TR Resolve<TR>(Type type) where TR : class;
        TR Resolve<TR>(Type type, string name) where TR : class;

        IEnumerable<T> ResolveAll<T>();
        IEnumerable<T> ResolveAll<T>(string name);
        IEnumerable<TR> ResolveAll<TR>(Type type) where TR : class;
        IEnumerable<TR> ResolveAll<TR>(Type type, string name) where TR : class;
    }
}
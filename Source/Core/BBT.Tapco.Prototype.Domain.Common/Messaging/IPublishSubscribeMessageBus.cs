﻿using BBT.Tapco.Prototype.Domain.Common.Messaging.Events;

namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public interface IPublishSubscribeMessageBus : IMessageBus
    {
        void Publish(object message);
        void Publish<T>(T message) where T : IBusEvent;
        void Subscribe<T>() where T : IBusEvent;
        void Subscribe<T>(IMessageHandler<T> callback) where T : IBusEvent;
    }
}
﻿using BBT.Tapco.Prototype.Domain.Common.Events;

namespace BBT.Tapco.Prototype.Domain.Common.Messaging.Events
{
    public interface IDomainToBusEventMap<T, TR> where T : IBusEvent where TR : IDomainEvent
    {
        TR Get(T @event);
        T Get(TR @event);
    }
}
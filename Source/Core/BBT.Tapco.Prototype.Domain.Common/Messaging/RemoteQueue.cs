﻿namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public sealed class RemoteQueue
    {
        public string QueueName { get; set; }
        public string MachineName { get; set; }
    }
}

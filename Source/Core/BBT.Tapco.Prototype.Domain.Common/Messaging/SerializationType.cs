namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public enum SerializationType
    {
        Json = 0,
        Xml,
        Binary
    }
}
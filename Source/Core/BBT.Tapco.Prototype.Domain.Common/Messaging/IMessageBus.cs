﻿using System;

namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public interface IMessageBus : IDisposable
    {
        void Configure(Configuration configuration);
    }
}


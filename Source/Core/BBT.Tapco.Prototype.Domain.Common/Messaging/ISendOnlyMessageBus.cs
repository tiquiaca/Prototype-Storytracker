﻿using BBT.Tapco.Prototype.Domain.Common.Messaging.Events;

namespace BBT.Tapco.Prototype.Domain.Common.Messaging
{
    public interface ISendOnlyMessageBus : IMessageBus
    {
        void Send<T>(T message) where T : IBusEvent;
        void Send<T>(RemoteQueue remoteQueue, T message) where T : IBusEvent;
        void Send<T>(string destination, T message) where T : IBusEvent;
    }
}
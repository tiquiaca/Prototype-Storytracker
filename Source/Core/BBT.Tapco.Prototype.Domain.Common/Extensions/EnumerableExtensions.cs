﻿using System;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.Domain.Common.Extensions
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (T element in enumerable)
            {
                action(element);
            }
        }
    }
}
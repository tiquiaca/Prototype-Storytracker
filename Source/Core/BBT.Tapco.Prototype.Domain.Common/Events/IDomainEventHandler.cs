﻿namespace BBT.Tapco.Prototype.Domain.Common.Events
{
    public interface IDomainEventHandler<in T> : IHandlerMarker where T : IDomainEvent
    {
        void Handle(T @event);
    }
}

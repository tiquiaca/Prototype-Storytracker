﻿using System;

namespace BBT.Tapco.Prototype.Domain.Common.Events
{
    public interface IDomainEvent
    {
        Guid Id { get; }
    }
}

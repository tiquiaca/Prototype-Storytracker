﻿using System.Collections.Generic;
using System.Linq;
using BBT.Tapco.Prototype.Domain.Common.Extensions;

namespace BBT.Tapco.Prototype.Domain.Common.Events.Dispatcher
{
    public class EventDispatcher : IEventDispatcher
    {
        private readonly IList<IHandlerMarker> _handlers;

        public EventDispatcher()
        {
            _handlers = new List<IHandlerMarker>();
        }

        public void Register<TEvent>(IDomainEventHandler<TEvent> handler) where TEvent : IDomainEvent
        {
            AssertionConcern.AssertArgumentNotNull(handler, "Cannot register a null handler!");
            _handlers.Add(handler);
        }

        public void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent
        {
            AssertionConcern.AssertArgumentNotNull(eventToDispatch, "Cannot dispatch a null event!");
            var handlers = _handlers.OfType<IDomainEventHandler<TEvent>>();
            handlers?.ForEach(handler => handler.Handle(eventToDispatch));
        }
    }
}
﻿using System;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters
{
    public class CharacterApplicationService : ICharacterApplicationService
    {
        private readonly ICharacterRepository _characterRepository;

        public CharacterApplicationService()
        {

        }
        public CharacterApplicationService(ICharacterRepository characterRepository)
:this()
        {
            _characterRepository = characterRepository;
        }

        public void NewCharacterWith(NewCharacterCommand newCharacterCommand)
        {
            // todo needs consistency guarantees here jshelton
            CharacterId id = _characterRepository.GetNextCharacterId();
            Character character = new Character(id, newCharacterCommand.Name, newCharacterCommand.Side);
            _characterRepository.Add(character);
        }

        #region Alex Task on Services
        public IEnumerable<Character> GetAllCharacters()
        {
            return _characterRepository.Get();
        }

        public Character GetCharacterById(int id)
        {
            return _characterRepository.GetCharacterById(id);
        }

        public bool AddCharacter(Character character)
        {
            if (character != null)
            {
                _characterRepository.Add(character);
                return true;
            }

            return false;
        }

        public void UpdateCharacter(Character character)
        {
            _characterRepository.Update(character);
        }

        public bool DeleteCharacter(Character character)
        {
            if(character != null)
            {
                _characterRepository.Delete(character);
                return true;
            }

            return false;
        }
        #endregion
    }
}
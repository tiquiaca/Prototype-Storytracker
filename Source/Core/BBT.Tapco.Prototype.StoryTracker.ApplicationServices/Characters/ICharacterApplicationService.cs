using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters
{
    public interface ICharacterApplicationService
    {
        void NewCharacterWith(NewCharacterCommand newCharacterCommand);

        #region Alex Task on Character Service
        IEnumerable<Character> GetAllCharacters();
        Character GetCharacterById(int id);
        bool AddCharacter(Character character);
        void UpdateCharacter(Character character);
        bool DeleteCharacter(Character character);
        #endregion
    }
}
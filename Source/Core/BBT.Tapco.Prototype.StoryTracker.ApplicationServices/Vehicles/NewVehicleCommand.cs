namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles
{
    public class NewVehicleCommand
    {
        public NewVehicleCommand()
        {
            
        }

        public NewVehicleCommand(string name, string type)
        {
            Name = name;
            Type = type;
        }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}
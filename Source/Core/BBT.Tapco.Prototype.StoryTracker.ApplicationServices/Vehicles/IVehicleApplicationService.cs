using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles
{
    public interface IVehicleApplicationService
    {
        bool NewVehicleWith(NewVehicleCommand newVehicleCommand);
        IEnumerable<Vehicle> GetAllVehicle();
        Vehicle GetVehicleById(int id);
        bool AddVehicle(Vehicle vehicle);
        bool UpdateVehicle(Vehicle vehicle);
        bool DeleteVehicle(Vehicle vehicle);
    }
}
﻿using System;
using System.Collections.Generic;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;

namespace BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles
{
    public class VehicleApplicationService : IVehicleApplicationService
    {
        private readonly IVehicleRepository _vehicleRepository;

        public VehicleApplicationService(IVehicleRepository vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        public bool AddVehicle(Vehicle vehicle)
        {
            if(vehicle != null)
            {
                _vehicleRepository.Add(vehicle);
                return true;
            }

            return false;
            
        }

        public bool DeleteVehicle(Vehicle vehicle)
        {
            if(vehicle != null) {
                _vehicleRepository.Delete(vehicle);
                return true;
            }

            return false;
        }

        public IEnumerable<Vehicle> GetAllVehicle()
        {
            return _vehicleRepository.Get();
        }

        public Vehicle GetVehicleById(int id)
        {
            return _vehicleRepository.GetVehicleById(id);
        }

        public bool NewVehicleWith(NewVehicleCommand newVehicleCommand)
        {
            if(newVehicleCommand != null)
            {
                VehicleId id = _vehicleRepository.GetNextVehicleId();
                Vehicle vehicle = new Vehicle(id, newVehicleCommand.Name, newVehicleCommand.Type);
                _vehicleRepository.Add(vehicle);
                return true;
            }
            return false;
        }

        public bool UpdateVehicle(Vehicle vehicle)
        {
            if(vehicle != null) {
                _vehicleRepository.Update(vehicle);
                return true;
            }
            return false;
        }
    }
}

﻿using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BBT.Tapco.Prototype.StoryTracker.Tests.Core.Unit
{
    public class CharacterServiceTests
    {
        private Mock<ICharacterRepository> _iCharacterRepository;
        private CharacterApplicationService characterApplicationService;

        [Fact]
        public void AddCharacter_WhenCharacterIsNotValid_ReturnFalse()
        {
            //arrange or setup
            _iCharacterRepository = new Mock<ICharacterRepository>();
            _iCharacterRepository.Setup(x => x.Add(null)).Returns(false);
            characterApplicationService = new CharacterApplicationService(_iCharacterRepository.Object);
            //act
            var result = characterApplicationService.AddCharacter(null);

            //assert
            Assert.Equal(false, result);
        }

        [Fact]
        public void AddCharacter_WhenCharacterIsValid_ReturnTrue()
        {
            //arrange or setup
            _iCharacterRepository = new Mock<ICharacterRepository>();
            _iCharacterRepository.Setup(x => x.Add(null)).Returns(false);
            characterApplicationService = new CharacterApplicationService(_iCharacterRepository.Object);
            var character = new Character {  Name = "Darth Kebino", Side = "Dark Side" };
            //act
            var result = characterApplicationService.AddCharacter(character);

            //assert
            Assert.Equal(true, result);
        }

        [Fact]
        public void GetAllCharacters_RetrieveAllCharacters_ReturnCharacterList()
        {
            //arrange or setup
            var characterList = new List<Character>
            {
               new Character {  Name = "Darth Kebino", Side = "Dark Side" },
               new Character {  Name = "Sith Lord Kebino", Side = "Dark Side" },
               new Character {  Name = "Jedi Master Kebino", Side = "Light Side" }
            };

            _iCharacterRepository = new Mock<ICharacterRepository>();
            _iCharacterRepository.Setup(x => x.Get()).Returns(() => characterList);
            characterApplicationService = new CharacterApplicationService(_iCharacterRepository.Object);
            //act
            var result = characterApplicationService.GetAllCharacters();

            //assert
            Assert.Equal(characterList, result);
        }

        [Fact]
        public void GetCharacterById_WhenIdExists_RetrieveCharacter()
        {
            var character = new Character { CharacterId = new CharacterId(1), Name = "Darth Kebino", Side = "Dark Side" };

            _iCharacterRepository = new Mock<ICharacterRepository>();
            _iCharacterRepository.Setup(x => x.GetCharacterById(1)).Returns(character);
            characterApplicationService = new CharacterApplicationService(_iCharacterRepository.Object);
            //act
            var result = characterApplicationService.GetCharacterById(1);

            //assert
            Assert.Equal(character, result);
        }

        [Fact]
        public void GetCharacterById_WhenIdNotExists_ReturnNull()
        {
            var character = new Character { CharacterId = new CharacterId(1), Name = "Darth Kebino", Side = "Dark Side" };

            _iCharacterRepository = new Mock<ICharacterRepository>();
            _iCharacterRepository.Setup(x => x.GetCharacterById(1)).Returns(character);
            characterApplicationService = new CharacterApplicationService(_iCharacterRepository.Object);
            //act
            var result = characterApplicationService.GetCharacterById(2);

            //assert
            Assert.Equal(null, result);
        }

        [Fact]
        public void Delete_WhenCharacterIsValid_ReturnTrue()
        {
            var character = new Character { CharacterId = new CharacterId(1), Name = "Darth Kebino", Side = "Dark Side" };

            _iCharacterRepository = new Mock<ICharacterRepository>();
            _iCharacterRepository.Setup(x => x.Delete(character)).Returns(true);
            characterApplicationService = new CharacterApplicationService(_iCharacterRepository.Object);
            //act
            var result = characterApplicationService.DeleteCharacter(character);

            //assert
            Assert.Equal(true, result);
        }

        [Fact]
        public void Delete_WhenCharacterIsNotValid_ReturnFalse()
        {
            var character = new Character { CharacterId = new CharacterId(1), Name = "Darth Kebino", Side = "Dark Side" };

            _iCharacterRepository = new Mock<ICharacterRepository>();
            _iCharacterRepository.Setup(x => x.Delete(character)).Returns(true);
            characterApplicationService = new CharacterApplicationService(_iCharacterRepository.Object);
            //act
            var result = characterApplicationService.DeleteCharacter(null);

            //assert
            Assert.Equal(false, result);
        }
    }
}

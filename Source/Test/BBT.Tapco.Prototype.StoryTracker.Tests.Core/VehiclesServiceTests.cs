﻿using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BBT.Tapco.Prototype.StoryTracker.Tests.Core.Unit
{
    public class VehiclesServiceTests
    {
        Mock<IVehicleRepository> _iVehicleRepository;
        VehicleApplicationService _vehicleApplicationService;

        [Fact]
        public void AddVehicle_WhenVehicleIsValid_ReturnTrue()
        {
            //arrange
            var vehicle = new Vehicle { Name="AT-AT", Type = "Land", VehicleId = new VehicleId(1)};
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.Add(vehicle)).Returns(true);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.AddVehicle(vehicle);

            //assert
            Assert.Equal(true, result);
        }

        [Fact]
        public void AddVehicle_WhenVehicleIsNotValid_ReturnFalse()
        {
            //arrange
            var vehicle = new Vehicle { Name = "AT-AT", Type = "Land", VehicleId = new VehicleId(1) };
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.Add(vehicle)).Returns(true);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.AddVehicle(null);

            //assert
            Assert.Equal(false, result);
        }

        [Fact]
        public void DeleteVehicle_WhenVehicleIsValid_ReturnTrue()
        {
            //arrange
            var vehicle = new Vehicle { Name = "AT-AT", Type = "Land", VehicleId = new VehicleId(1) };
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.Delete(vehicle)).Returns(true);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.DeleteVehicle(vehicle);

            //assert
            Assert.Equal(true, result);
        }

        [Fact]
        public void DeleteVehicle_WhenVehicleIsNotValid_ReturnFalse()
        {
            //arrange
            var vehicle = new Vehicle { Name = "AT-AT", Type = "Land", VehicleId = new VehicleId(1) };
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.Delete(vehicle)).Returns(true);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.DeleteVehicle(null);

            //assert
            Assert.Equal(false, result);
        }

        [Fact]
        public void GetAllVehicle_RetrieveVehicles_ReturnVehicleList()
        {
            //arrange
            var vehicle = new Vehicle { Name = "AT-AT", Type = "Land", VehicleId = new VehicleId(1) };
            var vehicleList = new List<Vehicle> { vehicle, vehicle};
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.Get()).Returns(() => vehicleList);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.GetAllVehicle();

            //assert
            Assert.Equal(vehicleList, result);
        }

        [Fact]
        public void GetVehicleById_WhenIdExists_ReturnVehicle()
        {
            //arrange
            var vehicle = new Vehicle { Name = "AT-AT", Type = "Land", VehicleId = new VehicleId(1) };
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.GetVehicleById(1)).Returns(vehicle);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.GetVehicleById(1);

            //assert
            Assert.Equal(vehicle, result);
        }

        [Fact]
        public void GetVehicleById_WhenIdNotExists_ReturnNull()
        {
            //arrange
            var vehicle = new Vehicle { Name = "AT-AT", Type = "Land", VehicleId = new VehicleId(1) };
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.GetVehicleById(1)).Returns(vehicle);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.GetVehicleById(2);

            //assert
            Assert.Equal(null, result);
        }

        [Fact]
        public void UpdateVehicle_WhenVehicleExist_ReturnTrue()
        {
            //arrange
            var vehicle = new Vehicle { Name = "AT-AT", Type = "Land", VehicleId = new VehicleId(1) };
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.Update(vehicle)).Returns(true);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.UpdateVehicle(vehicle);

            //assert
            Assert.Equal(true, result);
        }

        [Fact]
        public void UpdateVehicle_WhenVehicleNotExist_ReturnFalse()
        {
            //arrange
            var vehicle = new Vehicle { Name = "AT-AT", Type = "Land", VehicleId = new VehicleId(1) };
            _iVehicleRepository = new Mock<IVehicleRepository>();
            _iVehicleRepository.Setup(x => x.Update(vehicle)).Returns(true);
            _vehicleApplicationService = new VehicleApplicationService(_iVehicleRepository.Object);

            //act
            var result = _vehicleApplicationService.UpdateVehicle(null);

            //assert
            Assert.Equal(false, result);
        }
    }
}

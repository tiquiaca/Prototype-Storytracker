﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Moq;
using BBT.Tapco.Prototype.StoryTracker.Domain;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories.Mocks;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;

namespace BBT.Tapco.Prototype.StoryTracker.Tests.Api.Unit
{
    public class VehicleTests
    {
        private VehicleRepository _vehicleRepository;

        public VehicleTests()
        {
            _vehicleRepository = new VehicleRepository();  
        }

        [Fact]
        public void Add_InsertVehicleToRepository_VehicleIsInsertedToVehicleRepository()
        {
            //setup
        }

        [Fact]
        public void GetVehicleById_VehicleIsNotNull_RetreiveFromVehicleRepository()
        {
            //setup
            
        }

        [Fact]
        public void Get_ThrowsNotImplementedException_RetrieveVehicleRepository()
        {
            Assert.Throws<NotImplementedException>(() => _vehicleRepository.Get());
        }

        [Fact]
        public void GetNextVehicleId_ThrowsException_RetrieveFromVehicleRepository()
        {
            Assert.Throws<NotImplementedException>(() => _vehicleRepository.GetNextVehicleId());
        }

        [Fact]
        public void Update_ThrowsException_VehicleRepository()
        {
            Assert.Throws<NotImplementedException>(() => _vehicleRepository.Update(new Vehicle()));
        }
    }
}

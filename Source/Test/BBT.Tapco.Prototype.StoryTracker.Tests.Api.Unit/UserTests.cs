﻿using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BBT.Tapco.Prototype.StoryTracker.Tests.Api.Unit
{
    public class UserTests
    {
        [Fact]
        public void Add_AddUser_InsertUserToUserRepository()
        {

        }

        [Fact]
        public void Delete_DeleteUser_DeleteUserFromRepository()
        {

        }

        [Fact]
        public void GetUserById_UserNotNull_RetrieveUserFromRepository()
        {

        }

        [Fact]
        public void Update_UpdateUserData_UpdateUserInTheRepository()
        {

        }

        [Fact]
        public void Get_GetAllUsers_RetrieveUserFromRepository()
        {

        }

        [Fact]
        public void GetNextUserId_IncrementLastUserIdByOne_RetrieveLastUserIdFromRepositrory()
        {
           
        }
    }
}

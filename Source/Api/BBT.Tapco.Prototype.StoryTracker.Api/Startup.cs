﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Validation;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories.Mocks;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles;
using Autofac;
using System;
using Autofac.Extensions.DependencyInjection;
using NLog.Targets;
using NLog;
using NLog.Extensions.Logging;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.DotNet.InternalAbstractions;
using System.Runtime.Loader;

namespace BBT.Tapco.Prototype.StoryTracker.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            // Adding dependencies using built in DI
            /*
            services.AddScoped<ICharacterRepository, MockCharacterRepository>();
            services.AddScoped<ICharacterMapper, CharacterMapper>();
            services.AddScoped<ICharacterApplicationService, CharacterApplicationService>();
            services.AddScoped<INewCharacterCommandMapper, NewCharacterCommandMapper>();
            services.AddScoped<ICharacterValidator, CharacterValidator>();

            services.AddScoped<IVehicleRepository, MockVehicleRepository>();
            services.AddScoped<IVehicleMapper, VehicleMapper>();
            services.AddScoped<IVehicleApplicationService, VehicleApplicationService>();
            services.AddScoped<INewVehicleComamndMapper, NewVehicleComamndMapper>();
            services.AddScoped<IVehicleValidator, VehicleValidator>();
            */

            #region Alex's Task on Autofac
            // To do: Figure out how to do registration of all dependencies using Assembly
            var builder = new ContainerBuilder();

            //builder.RegisterType<MockCharacterRepository>().As<ICharacterRepository>();
            //builder.RegisterType<CharacterMapper>().As<ICharacterMapper>();
            //builder.RegisterType<CharacterApplicationService>().As<ICharacterApplicationService>();
            //builder.RegisterType<NewCharacterCommandMapper>().As<INewCharacterCommandMapper>();
            //builder.RegisterType<CharacterValidator>().As<ICharacterValidator>();

            //builder.RegisterType<MockVehicleRepository>().As<IVehicleRepository>();
            //builder.RegisterType<VehicleMapper>().As<IVehicleMapper>();
            //builder.RegisterType<VehicleApplicationService>().As<IVehicleApplicationService>();
            //builder.RegisterType<NewVehicleComamndMapper>().As<INewVehicleComamndMapper>();
            //builder.RegisterType<VehicleValidator>().As<IVehicleValidator>();
            
            var basePath = ApplicationEnvironment.ApplicationBasePath;

            //var commonAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath(new FileInfo($"{basePath + "/BBT.Tapco.Prototype.Domain.Common"}.dll").FullName);
            //var apiCommonAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath(new FileInfo($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Api.Common"}.dll").FullName);
            //var appServicesAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath(new FileInfo($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.ApplicationServices"}.dll").FullName);
            //var domainAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath(new FileInfo($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Domain"}.dll").FullName);
            //var dataAccessAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath(new FileInfo($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess"}.dll").FullName);
            //var mappersAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath(new FileInfo($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers"}.dll").FullName);
            //var validationAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath(new FileInfo($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation"}.dll").FullName);

            var commonAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath($"{basePath + "/BBT.Tapco.Prototype.Domain.Common"}.dll");
            var apiCommonAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Api.Common"}.dll");
            var appServicesAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.ApplicationServices"}.dll");
            var domainAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Domain"}.dll");
            var dataAccessAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess"}.dll");
            var mappersAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers"}.dll");
            var validationAsm = AssemblyLoadContext.Default.LoadFromAssemblyPath($"{basePath + "/BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation"}.dll");

            builder.RegisterAssemblyTypes(commonAsm).Where(t => t.Name.EndsWith("")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(apiCommonAsm).Where(t => t.Name.EndsWith("")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(appServicesAsm).Where(t => t.Name.EndsWith("")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(domainAsm).Where(t => t.Name.EndsWith("")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(dataAccessAsm).Where(t => t.Name.EndsWith("")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(mappersAsm).Where(t => t.Name.EndsWith("")).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(validationAsm).Where(t => t.Name.EndsWith("")).AsImplementedInterfaces();

            //task on removing concrete implementation of validators
            builder.RegisterType<CharacterValidator>().As<ICharacterValidator>();
            builder.RegisterType<VehicleValidator>().As<IVehicleValidator>();

            builder.Populate(services);

            var container = builder.Build();

            return container.Resolve<IServiceProvider>();
            #endregion
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            //add NLog
            loggerFactory.AddNLog();

            var configDir = "C:\\git\\Logs";

            if (configDir != string.Empty)
            {
                foreach (DatabaseTarget target in LogManager.Configuration.AllTargets.Where(t => t is DatabaseTarget))
                {
                    target.ConnectionString = Configuration.GetConnectionString("NLogDb");
                }

                var logEventInfo = NLog.LogEventInfo.CreateNullEvent();
                foreach (FileTarget target in LogManager.Configuration.AllTargets.Where(t => t is FileTarget))
                {
                    var filename = target.FileName.Render(logEventInfo).Replace("'", "");
                    target.FileName = Path.Combine(configDir, filename);
                }

                LogManager.ReconfigExistingLoggers();
            }

            if (env.IsDevelopment())
            {
                // Need to use status code pages
                // Need to use developer friendly execption pages
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}

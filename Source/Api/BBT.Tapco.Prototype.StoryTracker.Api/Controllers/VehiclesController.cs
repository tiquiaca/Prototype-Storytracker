﻿using BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Validation;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles;
using BBT.Tapco.Prototype.StoryTracker.Domain;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Api.Controllers
{
    //todo need authorization jshelton
    [Route("api/[controller]")]
    public class VehiclesController : Controller
    {
        private readonly IVehicleMapper _vehicleMapper;
        private readonly IVehicleApplicationService _vehicleApplicationService;
        private readonly INewVehicleComamndMapper _newVehicleCommandMapper;
        private readonly StoryTrackerEventSource _log = StoryTrackerEventSource.Log;
        private readonly IVehicleValidator _vehicleValidator;
        private readonly VehicleValidator _validator;

        private ILogger<VehiclesController> _logger;

        public VehiclesController(IVehicleRepository vehicleRepository, IVehicleMapper vehicleMapper,
            IVehicleValidator vehicleValidator,
            IVehicleApplicationService vehicleApplicationService,
            INewVehicleComamndMapper newVehicleCommandMapper,
            ILogger<VehiclesController> logger)
        {
            _logger = logger;
            _vehicleValidator = vehicleValidator;
            _vehicleMapper = vehicleMapper;
            _vehicleApplicationService = vehicleApplicationService;
            _newVehicleCommandMapper = newVehicleCommandMapper;
            _validator = new VehicleValidator();
        }

        // GET api/values
        [HttpGet]
        public IActionResult GetVehicles()
        {
            try
            {
                _log.StartTheRequestWasReceivedToReturnEveryVehicle();
                var vehicles = _vehicleApplicationService.GetAllVehicle();
                return Ok(vehicles);
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToReturnEveryVehicle(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while retrieving the characters");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToReturnEveryVehicle();
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                _log.StartTheRequestWasReceivedToReturnEveryVehicle();
                var vehicle = _vehicleApplicationService.GetVehicleById(id);
                if (vehicle == null) return NotFound();
                return Ok(vehicle);
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToReturnEveryVehicle(e.Message, e.StackTrace);
                return StatusCode(500, "An error occured while retrieving the characters");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToReturnEveryVehicle();

            }
            
            
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Common.Resources.Vehicle vehicle)
        {
            try
            {
                _log.StartTheRequestWasReceivedToAddVehicle();
                //IEnumerable<string> validation;
                //if (!_characterValidator.Validate(character, out validation))
                //    return BadRequest(validation);
                var result = _validator.Validate(vehicle);
                if (!result.IsValid) return BadRequest(result.Errors);

                //NewVehicleCommand newVehicleCommand = _newVehicleCommandMapper.Get(vehicle);
                //_vehicleApplicationService.NewVehicleWith(newVehicleCommand);
                _vehicleApplicationService.AddVehicle(_vehicleMapper.Get(vehicle));
                return Ok();
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToAddVehicle(e.Message,e.StackTrace);
                return StatusCode(500, "An error occurred while saving the character.");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToAddVehicle();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Common.Resources.Vehicle vehicle)
        {
            try
            {
                _log.StartTheRequestWasReceivedToDeleteVehicle();
                var model = _vehicleApplicationService.GetVehicleById(vehicle.Id);
                if (model == null) return NotFound();

                _vehicleApplicationService.DeleteVehicle(model);
                return Ok();
            }
            catch (Exception e)
            {
                _log.ErrorWhileExecutingRequestToDeleteVehicle(e.Message, e.StackTrace);
                return BadRequest();
            }
            finally
            {
                _log.StopTheRequestWasReceivedToDeleteCharacter();
            }
        }

        /// <summary>
        /// This is used to update the vehicle and use service to update and save changes
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Common.Resources.Vehicle vehicle)
        {
            try
            {
                _log.StartTheRequestWasReceivedToUpdateVehicle();
                var result = _validator.Validate(vehicle);
                if (!result.IsValid) return BadRequest(result.Errors);

                _vehicleApplicationService.UpdateVehicle(_vehicleMapper.Get(vehicle));

                return Ok();
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToUpdateVehicle(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while saving the character.");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToDeleteVehicle();
            }
        }
    }
}

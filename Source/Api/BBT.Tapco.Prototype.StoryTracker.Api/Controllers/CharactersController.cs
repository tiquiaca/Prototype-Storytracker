﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Validation;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;
using BBT.Tapco.Prototype.StoryTracker.Domain;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using Microsoft.Extensions.Logging;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation;

namespace BBT.Tapco.Prototype.StoryTracker.Api.Controllers
{
    //todo need authorization jshelton
    [Route("api/[controller]")]
    public class CharactersController : Controller
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly ICharacterMapper _characterMapper;
        private readonly ICharacterApplicationService _characterApplicationService;
        private readonly INewCharacterCommandMapper _newCharacterCommandMapper;
        private readonly StoryTrackerEventSource _log = StoryTrackerEventSource.Log;
        private readonly ICharacterValidator _characterValidator;
        private readonly CharacterValidator _validator;
        private ILogger<CharactersController> _logger;

        public CharactersController(ICharacterRepository characterRepository, ICharacterMapper characterMapper, 
            ICharacterValidator characterValidator,
            ICharacterApplicationService characterApplicationService,
            INewCharacterCommandMapper newCharacterCommandMapper,
            ILogger<CharactersController> _logger)
        {
            _characterValidator = characterValidator;
            _characterRepository = characterRepository;
            _characterMapper = characterMapper;
            _characterApplicationService = characterApplicationService;
            _newCharacterCommandMapper = newCharacterCommandMapper;
        }

        // GET api/values
        [HttpGet]
        [Route("Get")]
        public IActionResult GetCharacters()
        {
            try
            {
                 _log.StartTheRequestWasReceivedToReturnEveryCharacter();
                //IEnumerable<Character> characters = _characterRepository.Get();
                //List<Character> characters= new List<Character>() {
                //    new Character() { Name = "Chewbacca", Side = "light" },
                //    new Character() { Name = "Rey", Side = "light" },
                //    new Character() { Name = "Finn (FN2187", Side = "light" },
                //    new Character() { Name = "Han Solo", Side = "light" },
                //    new Character() { Name = "Leia Organa", Side = "light" }
                //};

                var characters = _characterApplicationService.GetAllCharacters();//->

               // var resCharacters = characters.Select(chr => _characterMapper.Get(chr)).ToList();
                return Ok(characters);
            }
            catch (Exception e)
            {
                _log.ErrorWhileExecutingRequestToReturnEveryCharacter(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while retrieving the characters");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToReturnEveryCharacter();
            }
        }

        // GET api/values/5
        [HttpGet("Get/{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var character = _characterApplicationService.GetCharacterById(id);//->
                
                return Ok(character);
            }
            catch (Exception e)
            {
                _log.ErrorWhileExecutingRequestToReturnEveryCharacter(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while retrieving the characters");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToReturnEveryCharacter();
            }
        }

        // POST api/values
        [HttpPost("Add")]
        public IActionResult Post([FromBody] Common.Resources.Character character)
        {
            try
            {
                _log.StartTheRequestWasReceivedToAddCharacter();

                //IEnumerable<string> validation;
                //if (!_characterValidator.Validate(character, out validation))
                //    return BadRequest(validation);
                var result = _characterValidator.Validate(character);
                if (!result.IsValid) return BadRequest(result.Errors);
                
                //NewCharacterCommand newCharacterCommand = _newCharacterCommandMapper.Get(character);
                //_characterApplicationService.NewCharacterWith(newCharacterCommand);

                _characterApplicationService.AddCharacter(_characterMapper.Get(character));

                return Ok();
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToAddVehicle(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while saving the character.");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToAddCharacter();
            }
        }

        // PUT api/values/5
        [HttpPut("Update/{id}")]
        public IActionResult Put(int id, [FromBody]Common.Resources.Character character)
        {
            try
            {
                _log.StartTheRequestWasReceivedToUpdateCharacter();
                var result = _validator.Validate(character);
                if (!result.IsValid) return BadRequest(result.Errors);

                _characterApplicationService.UpdateCharacter(_characterMapper.Get(character));

                return Ok();
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToUpdateCharacter(e.Message, e.StackTrace);
                return StatusCode(500, "An error occurred while saving the character.");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToUpdateCharacter();
            }
        }

        // DELETE api/values/5
        [HttpDelete("Delete/{id}")]
        public IActionResult Delete(int id, [FromBody]Common.Resources.Character character)
        {
            try
            {
                _log.StartTheRequestWasReceivedToDeleteCharacter();
                //IEnumerable<string> validation;
                //if (!_characterValidator.Validate(character, out validation))
                //    return BadRequest(validation);

                var toDelete = _characterApplicationService.GetCharacterById(id);

                _characterApplicationService.DeleteCharacter(toDelete);

                return Ok();
            }
            catch(Exception e)
            {
                _log.ErrorWhileExecutingRequestToDeleteCharacter(e.Message,e.StackTrace);
                return StatusCode(500, "An error occurred while deleting the character.");
            }
            finally
            {
                _log.StopTheRequestWasReceivedToDeleteCharacter();
            }
        }
    }
}

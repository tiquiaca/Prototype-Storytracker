﻿using BBT.Tapco.Prototype.StoryTracker.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers
{
    public interface IUserMapper
    {
        Resources.User Get(User user);
        User Get(Resources.User user);
    }
}

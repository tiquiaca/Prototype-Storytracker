﻿using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers
{
    public interface IVehicleMapper
    {

        //Gio Medin Interface for mapping
        Resources.Vehicle Get(Vehicle character);
        Vehicle Get(Resources.Vehicle character);
        List<Resources.Vehicle> GetAll(List<Vehicle> vehicles);
        List<Vehicle> GetAll(List<Resources.Vehicle> vehicles);
    }
}

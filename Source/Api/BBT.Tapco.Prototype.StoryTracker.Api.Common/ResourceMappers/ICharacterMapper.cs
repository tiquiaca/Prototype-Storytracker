﻿using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;
using System.Collections.Generic;

namespace BBT.Tapco.Prototype.StoryTracker.Api.Common.ResourceMappers
{
    public interface ICharacterMapper
    {
        //Gio Medin Interface for mapping

        Resources.Character Get(Character character);
        Character Get(Resources.Character character);
        List<Resources.Character> GetAll(List<Character> characters);
        List<Character> GetAll(List<Resources.Character> characters);
    }
}

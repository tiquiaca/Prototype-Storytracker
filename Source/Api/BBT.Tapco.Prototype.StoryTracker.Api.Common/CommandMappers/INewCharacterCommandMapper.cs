﻿using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;

namespace BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers
{
    public interface INewCharacterCommandMapper
    {
        NewCharacterCommand Get(Resources.Character character);
    }
}

﻿using System.Collections.Generic;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Validation;
using FluentValidation;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation
{
    public class CharacterValidator : AbstractValidator<Character>, ICharacterValidator
    {
        public CharacterValidator()
        {
            RuleFor(c => c.Name).Cascade(CascadeMode.StopOnFirstFailure)
                                .NotEmpty().WithMessage("{PropertyName} must not be empty!")
                                .Length(3, 25).WithMessage("{PropertyName} must be 3 to 25 characters!");
            RuleFor(c => c.Side).Cascade(CascadeMode.StopOnFirstFailure)
                                .NotEmpty().WithMessage("{PropertyName} must not be empty!")
                                .Length(3, 25).WithMessage("{PropertyName} must be 3 to 25 characters!");
        }
        //public bool Validate(Character character, out IEnumerable<string> message)
        //{
            
        //}
    }
}

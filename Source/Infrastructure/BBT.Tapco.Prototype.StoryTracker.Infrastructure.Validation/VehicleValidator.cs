﻿using System.Collections.Generic;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Validation;
using FluentValidation;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.Validation
{
    public class VehicleValidator : AbstractValidator<Vehicle>, IVehicleValidator
    {
        public VehicleValidator()
        {
            RuleFor(v => v.Name).Cascade(CascadeMode.StopOnFirstFailure)
                               .NotEmpty().WithMessage("Name must not be null")
                               .Length(3, 25).WithMessage("Name must be 3 to 25 characters!");
            RuleFor(v => v.Type).Cascade(CascadeMode.StopOnFirstFailure)
                                .NotEmpty().WithMessage("Type must not be null")
                                .Length(3, 25).WithMessage("Name must be 3 to 25 characters!");
        }
    }
}

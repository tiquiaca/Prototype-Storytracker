﻿using System.Collections.Generic;
using System.Linq;
using Domain = BBT.Tapco.Prototype.StoryTracker.Domain;
using Persistence = BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Mappers
{
    public class CharacterMapper
    {
        public Domain.Characters.Character Get(Persistence.Character character)
        {
            return new Domain.Characters.Character()
            {
              CharacterId = new Domain.Characters.CharacterId(character.Id),
              Name = character.Name,
              Side = character.Side
            };
        }

        public Persistence.Character Get(Domain.Characters.Character character)
        {
            return new Persistence.Character()
            {
                Id = character.CharacterId.Identifier,
                Name = character.Name,
                Side = character.Side
            };
        }

        public List<Domain.Characters.Character> GetList(List<Persistence.Character> Characters)
        {
            var result = new List<Domain.Characters.Character>();
            foreach (var item in Characters)
            {
                result.Add(Get(item));
            }
            return result;
        }

        public List<Persistence.Character> GetList(List<Domain.Characters.Character> Characters)
        {
            var result = new List<Persistence.Character>();
            foreach (var item in Characters)
            {
                result.Add(Get(item));
            }
            return result;

        }

    }
}
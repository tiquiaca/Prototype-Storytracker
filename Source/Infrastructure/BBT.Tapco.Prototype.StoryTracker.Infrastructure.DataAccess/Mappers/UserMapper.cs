﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Persistence = BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Mappers
{/// <summary>
/// Licuanan, John James
/// 12/21/2016
/// Class for User mapping.
/// </summary>
    public class UserMapper
    {
        public Domain.Users.User Get(Persistence.User user)
        {
            return null;
        }

        public Persistence.User Get(Domain.Users.User user)
        {
            return new Persistence.User()
            {
                UserId = user.UserId.Identifier,
                UserName = user.UserName,
                UserPassword = user.UserPassword,
                UserRole = user.UserRole
            };
        }


        public List<Domain.Users.User> GetList(List<Persistence.User> user)
        {
            var result = new List<Domain.Users.User>();
            foreach (var item in user)
            {
                result.Add(Get(item));
            }
            return result;
        }

        public List<Persistence.User> GetList(List<Domain.Users.User> user)
        {
            var result = new List<Persistence.User>();
            foreach (var item in user)
            {
                result.Add(Get(item));
            }
            return result;

        }
    }
}

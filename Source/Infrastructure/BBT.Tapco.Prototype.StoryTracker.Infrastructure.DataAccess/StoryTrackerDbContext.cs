﻿/* Licuanan, John James F.
 * 12/21/2016
 * Added DbContext to mirror database.
 */

using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel;
using Microsoft.EntityFrameworkCore;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess
{
    public class StoryTrackerDbContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<User> Users { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=.\\sqlexpress;Initial Catalog=tapsysDb;Integrated Security=True;MultipleActiveResultSets=True",
                b => b.UseRowNumberForPaging());

        }
    }
}

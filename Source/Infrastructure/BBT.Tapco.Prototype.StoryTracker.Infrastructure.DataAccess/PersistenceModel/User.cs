﻿/* Licuanan, John James F.
 * 12/21/2016
 * Added Class for DbContext to use.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }

        public string UserPassword { get; set; }
        
        public string UserRole { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Mappers;
using Vehicle = BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel.Vehicle;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories
{
    public interface IVehiclePersistenceRepository : IRepository<Vehicle>
    {

    }

    public class VehiclePersistenceRepository : RepositoryBase<Vehicle>, IVehiclePersistenceRepository
    {
        public VehiclePersistenceRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }


    public class VehicleRepository : IVehicleRepository
    {
        private readonly IVehiclePersistenceRepository _iVehiclePersistenceRepository;

        public VehicleRepository() { }

        public VehicleRepository(IVehiclePersistenceRepository iVehiclePersistenceRepository) : this()
        {
            _iVehiclePersistenceRepository = iVehiclePersistenceRepository;
        }

        public bool Add(Domain.Vehicles.Vehicle vehicle)
        {
            if(vehicle != null)
            {
                var mapper = new VehicleMapper();
                var parsistenceVehicle = mapper.Get(vehicle);
                _iVehiclePersistenceRepository.Add(parsistenceVehicle);
                return true;
            }
            return false;
        }

        public bool Delete(Domain.Vehicles.Vehicle vehicle)
        {
            if(vehicle != null) {
                var mapper = new VehicleMapper();
                var parsistenceVehicle = mapper.Get(vehicle);
                _iVehiclePersistenceRepository.Delete(parsistenceVehicle);
                return true;
            }
            return false;
        }

        public IEnumerable<Domain.Vehicles.Vehicle> Get()
        {
            throw new NotImplementedException();
        }

        public Domain.Vehicles.Vehicle GetVehicleById(int id)
        {
            var mapper = new VehicleMapper();
            var vehicle = _iVehiclePersistenceRepository.GetById(id);
            return mapper.Get(vehicle);

        }

        public bool Update(Domain.Vehicles.Vehicle vehicle)
        {
            throw new NotImplementedException();
        }

        public VehicleId GetNextVehicleId()
        {
            throw new NotImplementedException();
        }
    }
}
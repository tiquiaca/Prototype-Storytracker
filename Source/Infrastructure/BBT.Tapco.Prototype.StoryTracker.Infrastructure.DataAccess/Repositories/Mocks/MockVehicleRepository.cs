﻿using System;
using System.Collections.Generic;
using System.Linq;
using BBT.Tapco.Prototype.StoryTracker.Domain.Vehicles;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories.Mocks
{
    public class MockVehicleRepository : IVehicleRepository
    {
        private readonly IList<Vehicle> _vehicles = new List<Vehicle>();

        public bool Add(Vehicle character)
        {
            if(character != null)
            {
                _vehicles.Add(character);
                return true;
            }
            return false;
        }

        public bool Update(Vehicle character)
        {
            Vehicle foundCharacter = _vehicles.FirstOrDefault(c => c.Equals(character));
            if (foundCharacter != null) {
                foundCharacter.Name = character.Name;
                foundCharacter.Type = character.Type;
                return true;
            }
            return false;
        }

        public bool Delete(Vehicle character)
        {
            Vehicle foundCharacter = _vehicles.FirstOrDefault(c => c.Equals(character));
            if (foundCharacter != null)
            {
                _vehicles.Remove(foundCharacter);
                return true;
            }
            return false;
        }

        public IEnumerable<Vehicle> Get()
        {
            return _vehicles;
        }

        public VehicleId GetNextVehicleId()
        {
            int identifier = _vehicles.Max(c => c.VehicleId.Identifier) + 1;
            return new VehicleId(identifier);
        }

        public Vehicle GetVehicleById(int id)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BBT.Tapco.Prototype.StoryTracker.Domain;
using BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Mappers;
using Character=BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.PersistenceModel.Character;
using BBT.Tapco.Prototype.StoryTracker.Domain.Characters;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.DataAccess.Repositories
{
    public interface ICharacterPersistenceRepository : IRepository<Character>
    {

    }

    public class CharacterPersistenceRepository : RepositoryBase<Character>, ICharacterPersistenceRepository
    {
        public CharacterPersistenceRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }


    public class CharacterRepository : ICharacterRepository
    {
        private readonly ICharacterPersistenceRepository _iCharacterPersistenceRepository;

        public bool Add(Domain.Characters.Character character)
        {
            if(character != null) {
                var mapper = new CharacterMapper();
                var parsistenceCharacter = mapper.Get(character);
                _iCharacterPersistenceRepository.Add(parsistenceCharacter);

                return true;
            }

            return false;
        }

        public bool Delete(Domain.Characters.Character character)
        {
            if(character != null)
            {
                var mapper = new CharacterMapper();
                var parsistenceCharacter = mapper.Get(character);
                _iCharacterPersistenceRepository.Delete(parsistenceCharacter);

                return true;
            }

            return false;
        }

        public IEnumerable<Domain.Characters.Character> Get()
        {
            throw new NotImplementedException();
        }

        public Domain.Characters.Character GetCharacterById(int id)
        {
            var mapper = new CharacterMapper();
            var character= _iCharacterPersistenceRepository.GetById(id);
            return mapper.Get(character);

        }

        public CharacterId GetNextCharacterId()
        {
            throw new NotImplementedException();
        }

        public void Update(Domain.Characters.Character character)
        {
            throw new NotImplementedException();
        }

    }
}

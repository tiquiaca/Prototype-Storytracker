﻿using BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Characters;
using AutoMapper;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.CommandMappers
{
    public class NewCharacterCommandMapper: INewCharacterCommandMapper
    {
        //Mapping for getting new Character

        public NewCharacterCommand Get(Character character)
        {

            var config = new MapperConfiguration(cfg =>
            
                cfg.CreateMap<Character, NewCharacterCommand>()
            );


            IMapper mapper = config.CreateMapper();

          NewCharacterCommand newCharacterCommand=  mapper.Map<NewCharacterCommand>(character);


            return newCharacterCommand;
            //return new NewCharacterCommand()
            //{
            //    Name = character.Name,
            //    Side = character.Side
            //};
        }
    }
}
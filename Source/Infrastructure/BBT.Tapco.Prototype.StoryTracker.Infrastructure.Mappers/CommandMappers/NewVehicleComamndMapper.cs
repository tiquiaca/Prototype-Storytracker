﻿using BBT.Tapco.Prototype.StoryTracker.Api.Common.CommandMappers;
using BBT.Tapco.Prototype.StoryTracker.Api.Common.Resources;
using BBT.Tapco.Prototype.StoryTracker.ApplicationServices.Vehicles;
using AutoMapper;

namespace BBT.Tapco.Prototype.StoryTracker.Infrastructure.Mappers.CommandMappers
{
    public class NewVehicleComamndMapper : INewVehicleComamndMapper
    {

        //Gio Medina Mapping for Getting New Vehicle
        public NewVehicleCommand Get(Vehicle vehicle)
        {
            var config = new MapperConfiguration(cfg =>
            
                cfg.CreateMap<Vehicle, NewVehicleCommand>()
            );

            

            IMapper mapper = config.CreateMapper();

            NewVehicleCommand newVehicleCommand = mapper.Map<NewVehicleCommand>(vehicle);

            return newVehicleCommand;




        }
    }
}